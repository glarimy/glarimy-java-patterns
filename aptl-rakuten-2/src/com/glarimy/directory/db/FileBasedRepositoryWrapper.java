package com.glarimy.directory.db;

import com.glarimy.directory.api.domain.Employee;
import com.someone.db.FileBasedRepository;

public class FileBasedRepositoryWrapper implements Repository {
	private FileBasedRepository target;

	public FileBasedRepositoryWrapper() {
		target = new FileBasedRepository();
	}

	@Override
	public int insert(Employee e) {
		return target.create(e);
	}

	@Override
	public Employee find(int id) {
		return (Employee) target.read(id);
	}

}
