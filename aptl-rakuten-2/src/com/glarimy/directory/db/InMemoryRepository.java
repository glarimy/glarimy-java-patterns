package com.glarimy.directory.db;

import com.glarimy.directory.api.domain.Employee;

public class InMemoryRepository implements Repository {
	private static InMemoryRepository INSTANCE = null;

	private InMemoryRepository() {
		System.out.println("cons");
	}

	public static InMemoryRepository getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryRepository();
		return INSTANCE;
	}

	@Override
	public int insert(Employee e) {
		return 0;
	}

	@Override
	public Employee find(int id) {
		return null;
	}

}
