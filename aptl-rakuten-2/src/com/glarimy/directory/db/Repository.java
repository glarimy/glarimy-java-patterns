package com.glarimy.directory.db;

import com.glarimy.directory.api.domain.Employee;

public interface Repository {
	public int insert(Employee e);
	public Employee find(int id);
}
