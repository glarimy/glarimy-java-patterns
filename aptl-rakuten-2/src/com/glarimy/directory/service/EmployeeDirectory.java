package com.glarimy.directory.service;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.ObjectFactory;
import com.glarimy.directory.api.domain.Employee;
import com.glarimy.directory.api.exceptions.DirectoryException;
import com.glarimy.directory.api.exceptions.EmployeeNotFoundException;
import com.glarimy.directory.api.exceptions.InvalidEmployeeException;
import com.glarimy.directory.db.Repository;

public class EmployeeDirectory implements Directory {
	private Repository repo;

	public EmployeeDirectory() throws Exception {
		repo = (Repository) ObjectFactory.get("repo");
	}

	@Override
	public int addEmployee(Employee employee) throws InvalidEmployeeException, DirectoryException {
		if (employee == null)
			throw new InvalidEmployeeException();
		return repo.insert(employee);
	}

	@Override
	public Employee findEmployee(int eid) throws EmployeeNotFoundException, DirectoryException {
		Employee e = (Employee) repo.find(eid);
		if (e == null)
			throw new EmployeeNotFoundException();
		return e;
	}

}
