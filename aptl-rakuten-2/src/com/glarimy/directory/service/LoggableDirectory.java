package com.glarimy.directory.service;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.domain.Employee;
import com.glarimy.directory.api.exceptions.DirectoryException;
import com.glarimy.directory.api.exceptions.EmployeeNotFoundException;
import com.glarimy.directory.api.exceptions.InvalidEmployeeException;

public class LoggableDirectory implements Directory {
	private Directory target;

	public LoggableDirectory(Directory target) {
		this.target = target;
	}

	@Override
	public int addEmployee(Employee employee) throws InvalidEmployeeException, DirectoryException {
		System.out.println("Entering addEmployee:  " + employee);
		int id = target.addEmployee(employee);
		System.out.println("Leaving addEmployee wth id: " + id);
		return id;
	}

	@Override
	public Employee findEmployee(int eid) throws EmployeeNotFoundException, DirectoryException {
		System.out.println("Entering findEmployee:  " + eid);
		Employee e = target.findEmployee(eid);
		System.out.println("Leaving findEmployee wth eid: " + e);
		return e;
	}

}
