package com.glarimy.singleton;

public class Application {
	public static void main(String[] args) throws Exception {
		Component component = Component.getInstance();
		Component other = Component.getInstance();
		if(component == other)
			System.out.println("Both are referring the same object");
		component.execute();
	}
}
