package com.glarimy.proxy;

public interface Calculator {

	public double compute(double p, double t, double r) throws Exception;

}
