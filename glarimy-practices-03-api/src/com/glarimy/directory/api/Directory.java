package com.glarimy.directory.api;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

public interface Directory {
	void add(Employee employee) throws InvalidEmployeeException, DirectoryException;

	Optional<Employee> find(int id) throws DirectoryException;

	List<Employee> search(Predicate<Employee> condition) throws DirectoryException;

	List<Long> phones() throws DirectoryException;

	List<Employee> list() throws DirectoryException;
}