package com.glarimy.visitor;

public class DollerItem extends Host {
	private double price;

	public DollerItem(double price) {
		this.price = price;
	}

	public double getPrice() {
		return price;
	}
}
