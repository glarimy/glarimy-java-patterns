package com.glarimy.visitor;

public abstract class Host {
	public double accept(Visitor visitor) {
		return visitor.visit(this);
	}
}
