package com.glarimy.visitor;

import java.util.ArrayList;
import java.util.List;

public class Application {
	public static void main(String[] args) throws Exception {
		Visitor visitor = new DiscountCalculator();
		List<Host> hosts = new ArrayList<>();
		hosts.add(new PoundItem(100));
		hosts.add(new PoundItem(500));
		hosts.add(new PoundItem(110));
		hosts.add(new DollerItem(250));
		hosts.add(new PoundItem(80));
		double total = 0;
		for (Host host : hosts)
			total += host.accept(visitor);
		System.out.println("Total : Rs." + total);
	}
}