package com.glarimy.visitor;

public interface Visitor {
	public double visit(Host item);
}
