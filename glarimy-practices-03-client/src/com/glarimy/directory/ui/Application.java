package com.glarimy.directory.ui;

import java.util.List;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.ObjectFactory;
import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.EmployeeNotFoundException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

public class Application {
	public static void main(String[] args) {
		try {
			Directory dir = (Directory) ObjectFactory.get("directory");
			dir.add(new Employee(1, "Krishna", 1234));
			dir.add(new Employee(2, "Mohan", 2345));

			Employee emp = dir.find(1).orElseThrow(() -> new EmployeeNotFoundException());
			System.out.println(emp);

			List<Employee> results = dir.search(employee -> employee.getName().startsWith("Moh"));

			for (Employee e : results)
				System.out.println(e.getName());

			List<Long> phones = dir.phones();
			System.out.println(phones);

		} catch (InvalidEmployeeException eve) {
			System.out.println("Invalid employee details");
			eve.printStackTrace();
		} catch (DirectoryException de) {
			System.out.println("Internal error");
			de.printStackTrace();
		} catch (Exception de) {
			System.out.println("Internal error");
			de.printStackTrace();
		}
	}
}
