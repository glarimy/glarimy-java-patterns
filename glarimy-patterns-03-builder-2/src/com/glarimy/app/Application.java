package com.glarimy.app;

import java.util.Scanner;

import com.glarimy.service.Order;
import com.glarimy.service.OrderBuilder;

public class Application {
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(System.in);

		OrderBuilder builder = new OrderBuilder();

		System.out.println("Good evening. What do you want to have for starters?");
		String item = scanner.next();
		System.out.println("For how many?");
		int pax = scanner.nextInt();
		builder.starters(item, pax);

		System.out.println("Your preferred main course? South Indian or North Indian?");
		item = scanner.next();
		System.out.println("For how many?");
		pax = scanner.nextInt();
		builder.maincourse(item, pax);

		System.out.println("Anything for disserts?");
		item = scanner.next();
		System.out.println("For how many?");
		pax = scanner.nextInt();
		builder.disserts(item, pax);

		Order order = builder.build();
		order.print();
	}
}
