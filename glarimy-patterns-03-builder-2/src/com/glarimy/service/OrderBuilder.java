package com.glarimy.service;

import java.util.HashMap;
import java.util.Map;

public class OrderBuilder {
	private Map<String, Integer> starters;
	private Map<String, Integer> maincourse;
	private Map<String, Integer> disserts;

	public OrderBuilder() {
		starters = new HashMap<>();
		maincourse = new HashMap<>();
		disserts = new HashMap<>();
	}

	public OrderBuilder starters(String item, int qty) {
		if (starters.containsKey(item))
			qty += starters.get(item);
		starters.put(item, qty);

		return this;
	}

	public OrderBuilder maincourse(String item, int qty) {
		if (maincourse.containsKey(item))
			qty += maincourse.get(item);
		maincourse.put(item, qty);

		return this;
	}

	public OrderBuilder disserts(String item, int qty) {
		if (disserts.containsKey(item))
			qty += disserts.get(item);
		disserts.put(item, qty);

		return this;
	}

	public Order build() {
		return new Order(starters, maincourse, disserts);
	}

}
