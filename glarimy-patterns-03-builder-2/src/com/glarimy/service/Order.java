package com.glarimy.service;

import java.util.Map;

public class Order {
	private Map<String, Integer> starters;
	private Map<String, Integer> maincourse;
	private Map<String, Integer> disserts;

	Order(Map<String, Integer> starters, Map<String, Integer> maincourse, Map<String, Integer> disserts) {
		this.starters = starters;
		this.maincourse = maincourse;
		this.disserts = disserts;
	}

	public void print() {
		System.out.println("STARTERS");
		for (String item : starters.keySet())
			System.out.println(item + " : " + starters.get(item));

		System.out.println("Main Course");
		for (String item : maincourse.keySet())
			System.out.println(item + " : " + maincourse.get(item));

		System.out.println("Disserts");
		for (String item : disserts.keySet())
			System.out.println(item + " : " + disserts.get(item));
	}

}
