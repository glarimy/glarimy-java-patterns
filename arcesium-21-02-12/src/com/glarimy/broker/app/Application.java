package com.glarimy.broker.app;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Date;
import java.util.Map;
import java.util.stream.IntStream;

import com.glarimy.broker.api.Broker;
import com.glarimy.broker.api.Handler;
import com.glarimy.broker.api.Message;
import com.glarimy.broker.api.MessageBuilder;
import com.glarimy.broker.service.MessageBroker;

public class Application {

	public static void main(String[] args) throws Exception {
		Broker broker = new MessageBroker();
		String id = broker.subscribe(new Handler() {

			@Override
			public void on(Message message) {
				Map<String, Object> headers = message.getHeaders();
				System.out.println(headers.get("timestamp") + " [" + headers.get("origin") + "]: " + message.getBody());
			}

			@Override
			public String getTopic() {
				return "com.glarimy.app.log";
			}
		});

		InetAddress address = Inet4Address.getLocalHost();

		IntStream.range(0, 10).forEach(n -> {
			Message message = new MessageBuilder("com.glarimy.app.log", "Message-" + n)
					.addHeader("timestamp", new Date()).addHeader("origin", address).build();
			broker.notify(message);
		});
		
		broker.unsubscribe(id);
		
		IntStream.range(0, 10).forEach(n -> {
			Message message = new MessageBuilder("com.glarimy.app.log", "Message-" + n)
					.addHeader("timestamp", new Date()).addHeader("origin", address).build();
			broker.notify(message);
		});
	}
}
