package com.glarimy.broker.api;

public interface Broker {
	public String subscribe(Handler handler);
	public void unsubscribe(String sid);
	public void notify(Message message);
}
