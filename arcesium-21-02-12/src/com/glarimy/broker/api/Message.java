package com.glarimy.broker.api;

import java.util.Map;

public interface Message {
	public String getTopic();
	public Map<String, Object> getHeaders();
	public Object getBody();
}
