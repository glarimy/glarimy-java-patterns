package com.glarimy.broker.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import com.glarimy.broker.api.Broker;
import com.glarimy.broker.api.Handler;
import com.glarimy.broker.api.Message;

public class MessageBroker implements Broker {
	private Map<String, List<String>> topics;
	private Map<String, BlockingQueue<Message>> queues;
	private Map<String, Thread> threads;
	private static final Random GENERATOR = new Random();

	public MessageBroker() {
		topics = new HashMap<>();
		threads = new HashMap<>();
		queues = new HashMap<>();
	}

	@Override
	public String subscribe(Handler handler) {
		if (!topics.containsKey(handler.getTopic()))
			topics.put(handler.getTopic(), new ArrayList<>());

		String id = GENERATOR.nextLong() + "";

		BlockingQueue<Message> queue = new LinkedBlockingDeque<>();
		queues.put(id, queue);

		Thread thread = new Thread(new Notifier(queue, handler));
		threads.put(id, thread);
		thread.start();

		topics.get(handler.getTopic()).add(id);

		return id;
	}

	@Override
	public void unsubscribe(String id) {
		threads.get(id).interrupt();
		try {
			threads.get(id).join();
		} catch (InterruptedException e) {
		} finally {
			threads.remove(id);
			queues.remove(id);
			for (List<String> ids : topics.values()) {
				if (ids.remove(id) == true)
					break;
			}
		}
	}

	@Override
	public void notify(Message message) {
		topics.get(message.getTopic()).stream().map(id -> queues.get(id)).forEach(q -> q.add(message));
	}

}