package com.glarimy.broker.service;

import java.util.concurrent.BlockingQueue;

import com.glarimy.broker.api.Handler;
import com.glarimy.broker.api.Message;

public class Notifier implements Runnable {
	private BlockingQueue<Message> queue;
	private Handler handler;

	public Notifier(BlockingQueue<Message> queue, Handler handler) {
		this.queue = queue;
		this.handler = handler;
	}

	@Override
	public void run() {
		try {
			while (true)
				handler.on(queue.take());
		} catch (InterruptedException e) {
			System.out.println("Shutting down");
		}
	}
}
