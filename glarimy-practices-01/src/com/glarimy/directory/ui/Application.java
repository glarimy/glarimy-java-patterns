package com.glarimy.directory.ui;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.DirectoryException;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.InvalidEmployeeException;
import com.glarimy.directory.service.SimpleDirectory;

public class Application {
	public static void main(String[] args) {
		Directory dir = new SimpleDirectory();
		try {
			dir.add(new Employee(1, "Krishna", 1234));
			dir.add(new Employee(2, "Mohan", 2345));
			System.out.println(dir.find(1));
			System.out.println(dir.search("Mohan"));
			System.out.println(dir.list());
		}catch(InvalidEmployeeException eve) {
			System.out.println("Invalid employee details");
		}catch(DirectoryException de) {
			System.out.println("Internal error");
		}
	}
}
