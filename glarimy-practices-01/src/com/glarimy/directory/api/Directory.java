package com.glarimy.directory.api;

import java.util.List;

public interface Directory {
	void add(Employee employee) throws InvalidEmployeeException, DirectoryException;

	Employee find(int id) throws EmployeeNotFoundException, DirectoryException;

	List<Employee> search(String name) throws DirectoryException;

	List<Employee> list() throws DirectoryException;
}