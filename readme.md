#Naming Conventions

1.  Class names: Nouns in camelCase with first letter in upper case
2.  Method Names: Action verbs in camelCase
3.  Variables: Nouns in camelCase
4.  Constants & Statics: UPPERCASE_WITH_UNDERSCORE
5.  Make code readable, avoid comments

#Principles

1.  SOLID: Single Responsibility, Open-Close, Liskov's Substitution, Interface Segregation and Dependency Inversion
2.  DRY: Don't Repeat Yourself

#Patterns

0.  For reusability by loose coupling and high cohesion
1.  Factory to take the responsibility of object creation from it's users
2.  Builder to build complex objects interactively
3.  Singleton to limit the number of instances of a class to maximum of one
4.  AbstractFactory is to choose a suitable factory for an interface
4.  Adapter to provide known interface to unknown objects. Prefer composition than inheritance
5.  Proxy to decorate objects anonymously
6.  Decorator to enhance an object
7.  CoR to escalate a call conditionally
9.  Composite is to treat the whole as sum of the parts
10. Strategy is to select an algorithm at runtime
11. Template Method is to provide abstraction of an algorithm which will be implemented by sub-classes
12. Visitor adds functionality and decorates object graph

#Practices

0.  Start with interfaces and API
1.  Use objects instead of comma separated multiple parameters
2.  Throw exceptions, instead of returning control using booleans and enums in case of failure
3.  Always code against interfaces
4.  Use reflection in frameworks. Think twice using reflection in applications.
5.  Provide annotations for meta-programming, in frameworks
6.  Choose thread-safe objects in multi-threaded apps
7.  Never attempt write operations on a collection through it's iterator
8.  Do not keep catch blocks empty
9.  Add delimiters to even single-line blocks as well
10. Know the difference between object-level locking Vs block-level locking

#Functional Programming

0.  Use functional interfaces and lambdas to pass functionality
1.  Use stream API
2.  Use Optionals