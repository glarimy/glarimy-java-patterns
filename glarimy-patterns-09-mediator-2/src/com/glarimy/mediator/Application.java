package com.glarimy.mediator;

public class Application {
	public static void main(String[] args) throws Exception {
		Mediator mediator = new Mediator();
		ConcreteMember member = new ConcreteMember(mediator);
		mediator.add(member, "Greetings");
		mediator.add(new ConcreteMember(mediator), "Greetings");
		mediator.add(new ConcreteMember(mediator), "Greetings");
		mediator.add(new ConcreteMember(mediator), "Sports");
		mediator.add(new ConcreteMember(mediator), "Sports");
		mediator.add(new ConcreteMember(mediator), "Sports");
		member.execute();
	}
}