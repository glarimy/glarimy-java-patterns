package com.glarimy.mediator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Mediator {
	private Map<String, List<Member>> members;

	public Mediator() {
		members = new HashMap<String, List<Member>>();
	}

	public void add(Member member, String topic) {
		List<Member> list = members.get(topic);
		if(list == null)
			list = new ArrayList<>();
		list.add(member);
		members.put(topic, list);
	}

	public void notify(String topic, String message) {
		List<Member> list = members.get(topic);
		for(Member member: list)
			member.onMessage(message);

	}
}
