package com.glarimy.quiz.service;

import java.util.ArrayList;
import java.util.List;

import com.glarimy.quiz.api.Question;
import com.glarimy.quiz.api.QuestionBank;

public class QuestionBankImpl implements QuestionBank {
	private String subject;
	private List<Question> entries = new ArrayList<>();

	public QuestionBankImpl() {
	}

	@Override
	public List<Question> fetch(int size) {
		return entries;
	}

	@Override
	public void add(Question entry) {
		entries.add(entry);
	}

	@Override
	public String getSubject() {
		return this.subject;
	}

	@Override
	public void setSubject(String subject) {
		this.subject = subject;
	}

}
