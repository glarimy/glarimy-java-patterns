package com.glarimy.quiz.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.glarimy.quiz.api.Question;
import com.glarimy.quiz.api.QuestionBank;
import com.glarimy.quiz.api.Quiz;

public class SimpleQuiz implements Quiz {
	private Map<String, QuestionBank> banks;

	public SimpleQuiz() {
		this.banks = new HashMap<String, QuestionBank>();
	}

	@Override
	public List<Question> start(String subject) {
		return banks.get(subject).fetch(20);
	}

	@Override
	public void add(QuestionBank bank) {
		banks.put(bank.getSubject(), bank);
	}

}
