package com.glarimy.quiz.api;

import java.util.List;

public interface QuestionBank {
	List<Question> fetch(int size);

	public String getSubject();

	public void setSubject(String subject);

	void add(Question entry);
}
