package com.glarimy.quiz.api;

import java.util.List;

public interface Quiz {
	void add(QuestionBank bank);
	List<Question> start(String subject);
}
