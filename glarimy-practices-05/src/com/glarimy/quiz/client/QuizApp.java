package com.glarimy.quiz.client;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.glarimy.quiz.api.Question;
import com.glarimy.quiz.api.QuestionBank;
import com.glarimy.quiz.api.Quiz;

public class QuizApp {

	public static void main(String[] args) throws Exception {

		Properties props = new Properties();
		props.load(new FileReader("conf.properties"));

		QuestionBank bank = 
				(QuestionBank) Class.forName(props.getProperty("bank")).newInstance();
		bank.setSubject("History");
		Question q = new Question();
		q.setDescription("Capital of India?");
		q.setOptionOne("Agra");
		q.setOptionTwo("Calcutta");
		q.setOptionThree("New Delhi");
		q.setQid(1);
		q.setAnswer(3);

		bank.add(q);

		Map<String, QuestionBank> banks = new HashMap<>();
		banks.put("History", bank);

		Quiz quiz = (Quiz) Class.forName(props.getProperty("quiz")).newInstance();
		quiz.add(bank);
		List<Question> questions = quiz.start("History");
		for (Question question : questions) {
			System.out.println(question.getDescription());
			System.out.println("1. " + question.getOptionOne());
			System.out.println("2. " + question.getOptionTwo());
			System.out.println("3. " + question.getOptionThree());
			int answer = 0;
			if (question.getAnswer() == answer)
				System.out.println("Correct");
			else
				System.out.println("Incorrect");
		}

	}

}
