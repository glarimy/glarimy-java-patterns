package com.glarimy.broker.api;

public interface Handler {
	public void on(Message message);
	public String getTopic();
}
