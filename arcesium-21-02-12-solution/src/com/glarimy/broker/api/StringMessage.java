package com.glarimy.broker.api;

import java.util.Map;

public class StringMessage implements Message {
	private String topic;
	private String body;
	private Map<String, Object> headers;

	StringMessage(String topic, String body, Map<String, Object> headers) {
		super();
		this.topic = topic;
		this.body = body;
		this.headers = headers;
	}

	@Override
	public String getTopic() {
		return topic;
	}

	@Override
	public Map<String, Object> getHeaders() {
		return headers;
	}

	@Override
	public Object getBody() {
		return body;
	}

}
