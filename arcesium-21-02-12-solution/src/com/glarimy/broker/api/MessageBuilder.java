package com.glarimy.broker.api;

import java.util.HashMap;
import java.util.Map;

public class MessageBuilder {
	private String body;
	private String topic;
	private Map<String, Object> headers;

	public MessageBuilder(String topic, String body) {
		this.topic = topic;
		this.body = body;
		this.headers = new HashMap<>();
	}

	public MessageBuilder addHeader(String header, Object value) {
		headers.put(header, value);
		return this;
	}

	public Message build() {
		return new StringMessage(topic, body, headers);
	}

}
