package com.glarimy.broker.service;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

import com.glarimy.broker.api.Handler;
import com.glarimy.broker.api.Message;

public class Notifier implements Runnable {
	private BlockingQueue<Message> queue;
	private Map<String, Map<String, Handler>> topics;


	public Notifier(BlockingQueue<Message> queue, Map<String, Map<String, Handler>> topics) {
		this.queue = queue;
		this.topics = topics;
	}

	@Override
	public void run() {
		try {
			while (true) {
				Message message = queue.take();
				for(Handler handler: topics.get(message.getTopic()).values())
					handler.on(message);
			}
		} catch (InterruptedException e) {
			System.out.println("Shutting down");
		}
	}
}
