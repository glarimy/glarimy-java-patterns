package com.glarimy.broker.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import com.glarimy.broker.api.Broker;
import com.glarimy.broker.api.Handler;
import com.glarimy.broker.api.Message;

public class MessageBroker implements Broker {
	private Map<String, Map<String, Handler>> topics;
	private BlockingQueue<Message> queue;
	private ExecutorService workers;
	private static final Random GENERATOR = new Random();

	public MessageBroker() {
		topics = new HashMap<>();
		queue = new LinkedBlockingDeque<>();
		workers = Executors.newFixedThreadPool(10);
		workers.submit(new Notifier(queue, topics));
	}

	@Override
	public String subscribe(Handler handler) {
		if (!topics.containsKey(handler.getTopic()))
			topics.put(handler.getTopic(), new HashMap<>());

		String id = GENERATOR.nextLong() + "";
		topics.get(handler.getTopic()).put(id, handler);
		return id;
	}

	@Override
	public void unsubscribe(String id) {
		for (Map<String, Handler> handlers : topics.values()) {
			if (handlers.remove(id) != null)
				break;
		}
	}

	@Override
	public void notify(Message message) {
		if (topics.containsKey(message.getTopic())) {
			queue.add(message);
		}
	}

}