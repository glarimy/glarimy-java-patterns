package com.glarimy.ui;

public class WindowBuilder {
	private Window window;

	public WindowBuilder() {
		window = new Window();
	}

	public WindowBuilder addTitle(String title) {
		window.title = title;
		return this;
	}

	public WindowBuilder addCloseButton() {
		window.close = true;
		return this;
	}

	public WindowBuilder addMinButton() {
		window.min = true;
		return this;
	}

	public WindowBuilder addMaxButton() {
		window.max = true;
		return this;
	}

	public WindowBuilder addScroll(boolean horizontal, boolean vertical) {
		window.hscroll = horizontal;
		window.vscroll = vertical;
		return this;
	}

	public Window build() {
		return window;
	}

}
