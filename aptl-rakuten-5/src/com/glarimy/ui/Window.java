package com.glarimy.ui;

public class Window {
	String title;
	boolean close;
	boolean min;
	boolean max;
	boolean hscroll;
	boolean vscroll;

	Window() {
		title = "New Window";
	}

	public void display() {
		System.out.println("Window : " + title);
		System.out.println("With the following features");
		System.out.println(close ? "Close Button" : "");
		System.out.println(min ? "Minimize Button" : "");
		System.out.println(max ? "Maximize Button" : "");
		System.out.println(hscroll ? "Horizontal Scroll" : "");
		System.out.println(vscroll ? "Vertical Scroll" : "");
	}

}
