package com.glarimy.ui;

public class UserInterface {
	public static void main(String[] args) {
		Window window = new WindowBuilder().addTitle("Editor").addScroll(false, true).build();
		window.display();
	}

}
