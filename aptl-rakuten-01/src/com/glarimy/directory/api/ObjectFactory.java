package com.glarimy.directory.api;

import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Properties;

public class ObjectFactory {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object get(String key) throws Exception {
		Class claz = null;
		Object o;
		Properties props = new Properties();
		try {
			props.load(new FileReader("factory.properties"));
			String className = props.getProperty(key);
			claz = Class.forName(className);
			o = claz.newInstance();
		} catch (IllegalAccessException iae) {
			Method method = claz.getMethod("getInstance");
			o = method.invoke(claz);
		} catch (Exception e) {
			throw e;
		}

		String decoratorName = props.getProperty(key + ".decorator");
		if (decoratorName == null)
			return o;
		else {
			Class[] interfaces = claz.getInterfaces();
			Class parent = interfaces[0];
			Class decoratorClass = Class.forName(decoratorName);
			Constructor cons = decoratorClass.getConstructor(parent);
			Object decorator = cons.newInstance(o);
			return decorator;
		}
	}

}
