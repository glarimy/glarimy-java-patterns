package com.glarimy.directory.api;

import com.glarimy.directory.api.domain.Employee;
import com.glarimy.directory.api.exceptions.DirectoryException;
import com.glarimy.directory.api.exceptions.EmployeeNotFoundException;
import com.glarimy.directory.api.exceptions.InvalidEmployeeException;

/**
 * 
 * @author glarimy
 *
 */

public interface Directory {
	/**
	 * 
	 * @param employee
	 * @return
	 * @throws InvalidEmployeeException
	 * @throws DirectoryException
	 */
	public int addEmployee(Employee employee) throws InvalidEmployeeException, DirectoryException;

	/**
	 * 
	 * @param eid
	 * @return
	 * @throws EmployeeNotFoundException
	 * @throws DirectoryException
	 */
	public Employee findEmployee(int eid) throws EmployeeNotFoundException, DirectoryException;
}
