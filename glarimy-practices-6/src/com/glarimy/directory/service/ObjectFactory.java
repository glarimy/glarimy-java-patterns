package com.glarimy.directory.service;

import java.io.FileReader;
import java.util.Properties;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.DirectoryException;

public class ObjectFactory {

	public static Object get(String key) {
		try {
			Properties props = new Properties();
			props.load(new FileReader("conf.properties"));
			if (key == "dir") {
				DirectoryBuilder builder = new DirectoryBuilder();
				if (Boolean.parseBoolean(props.getProperty("dir.log")) == true)
					builder.addLogger();
				if (Boolean.parseBoolean(props.getProperty("dir.security")) == true)
					builder.addSecurity();
				if (Boolean.parseBoolean(props.getProperty("dir.validation")) == true)
					builder.addValidator();
				return builder.build();

			}
			if (key == "adapter") {
				return new CacheAdapter();
			}
			throw new DirectoryException();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DirectoryException();
		}

	}

}
