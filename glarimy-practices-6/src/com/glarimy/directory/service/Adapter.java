package com.glarimy.directory.service;

import java.util.List;

public interface Adapter<T> {
	public void create(int key, T value);
	public T read(int key);
	public List<T> values();
}
