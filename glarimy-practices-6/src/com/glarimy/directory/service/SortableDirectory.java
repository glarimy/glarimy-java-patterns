package com.glarimy.directory.service;

import java.util.Comparator;
import java.util.List;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;

public class SortableDirectory implements Directory {
	private Directory target;

	public SortableDirectory(Directory target) {
		super();
		this.target = target;
	}

	@Override
	public int add(Employee employee) {
		return target.add(employee);
	}

	@Override
	public Employee find(int eid) {
		return target.find(eid);
	}

	@Override
	public List<Employee> list() {
		return target.list();
	}

	public List<Employee> sortedList() {
		List<Employee> employees = target.list();
		employees.sort(new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		return employees;
	}

}
