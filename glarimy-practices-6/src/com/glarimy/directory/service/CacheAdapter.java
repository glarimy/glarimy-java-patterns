package com.glarimy.directory.service;

import java.util.List;

import com.glarimy.directory.api.Employee;
import com.someone.Cache;

public class CacheAdapter implements Adapter<Employee> {
	private Cache<Employee> cache = new Cache<>();

	@Override
	public void create(int key, Employee value) {
		cache.put(key, value);

	}

	@Override
	public Employee read(int key) {
		return cache.get(key);
	}

	@Override
	public List<Employee> values() {
		return cache.all();
	}
}
