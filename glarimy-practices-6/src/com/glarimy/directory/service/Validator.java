package com.glarimy.directory.service;

import java.util.List;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.InvalidEmployeeException;

public class Validator implements Directory {
	private Directory target;

	public Validator(Directory target) {
		this.target = target;
	}

	@Override
	public int add(Employee employee) {
		if (employee == null)
			throw new InvalidEmployeeException();
		return target.add(employee);
	}

	@Override
	public Employee find(int eid) {
		return target.find(eid);
	}

	@Override
	public List<Employee> list() {
		List<Employee> list = target.list();
		return list;
	}

}
