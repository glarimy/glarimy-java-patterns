package com.glarimy.directory.service;

import com.glarimy.directory.api.Directory;

public class DirectoryBuilder {
	private boolean logger;
	private boolean validator;
	private boolean security;

	public DirectoryBuilder addLogger() {
		logger = true;
		return this;
	}

	public DirectoryBuilder addValidator() {
		validator = true;
		return this;
	}

	public DirectoryBuilder addSecurity() {
		security = true;
		return this;
	}

	public Directory build() {
		Directory dir = InMemoryDirectory.getInstance();
		if (logger == true) {
			dir = new Logger(dir);
		}
		if (security == true) {
			dir = new Security(dir);
		}
		if (validator == true) {
			dir = new Validator(dir);
		}
		return dir;
	}
}
