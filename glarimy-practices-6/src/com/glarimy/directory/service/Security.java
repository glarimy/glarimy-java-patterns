package com.glarimy.directory.service;

import java.util.List;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;

public class Security implements Directory {
	private Directory target;

	public Security(Directory target) {
		this.target = target;
	}

	@Override
	public int add(Employee employee) {
		System.out.println("Authenticating ...");
		int id = target.add(employee);
		return id;
	}

	@Override
	public Employee find(int eid) {
		System.out.println("Authenticating: " + eid);
		Employee e = target.find(eid);
		return e;
	}

	@Override
	public List<Employee> list() {
		System.out.println("Authenticating: ");
		List<Employee> list = target.list();
		return list;
	}

}
