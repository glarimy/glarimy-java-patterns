package com.glarimy.directory.service;

import java.util.List;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.EmployeeNotFoundException;

public class InMemoryDirectory implements Directory {
	@SuppressWarnings("unchecked")
	private Adapter<Employee> adapter = (Adapter<Employee>) ObjectFactory.get("adapter");
	private static InMemoryDirectory INSTANCE = null;

	private InMemoryDirectory() {
	}

	public static synchronized InMemoryDirectory getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryDirectory();
		return INSTANCE;
	}

	@Override
	public int add(Employee employee) {
		int id = adapter.values().size();
		adapter.create(id, employee);
		return id;
	}

	@Override
	public Employee find(int eid) {
		Employee e = adapter.read(eid);
		if (e == null)
			throw new EmployeeNotFoundException();
		return e;
	}

	@Override
	public List<Employee> list() {
		return adapter.values();
	}

}
