package com.glarimy.directory.service;

import java.util.List;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;

public class Logger implements Directory {
	private Directory target;

	public Logger(Directory target) {
		this.target = target;
	}

	@Override
	public int add(Employee employee) {
		System.out.println("Adding " + employee);
		int id = target.add(employee);
		System.out.println("Added employee with id: " + id);
		return id;
	}

	@Override
	public Employee find(int eid) {
		System.out.println("Finding employee with id: " + eid);
		Employee e = target.find(eid);
		System.out.println("Found: " + e);
		return e;
	}

	@Override
	public List<Employee> list() {
		System.out.println("Fetching list of employees");
		List<Employee> list = target.list();
		System.out.println("Fetched " + list.size() + " employees");
		return list;
	}

}
