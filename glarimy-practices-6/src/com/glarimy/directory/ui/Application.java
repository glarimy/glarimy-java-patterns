package com.glarimy.directory.ui;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.service.ObjectFactory;
import com.glarimy.directory.service.SortableDirectory;

public class Application {
	public static void main(String[] args) {
		Directory dir = (Directory) ObjectFactory.get("dir");
		
		dir.add(new Employee("Krishna", 9731423166L));
		dir.add(new Employee("Mohan", 9731423166L));
		dir.add(new Employee("Koyya", 9731423166L));

		System.out.println(dir.list());
		
		SortableDirectory sd = new SortableDirectory(dir);
		System.out.println(sd.sortedList());
	}
}
