package com.glarimy.directory.api;

public class DirectoryException extends RuntimeException {
	private static final long serialVersionUID = 709259737975435637L;

	public DirectoryException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DirectoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DirectoryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DirectoryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DirectoryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
