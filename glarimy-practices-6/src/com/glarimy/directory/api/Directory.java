package com.glarimy.directory.api;

import java.util.List;

public interface Directory {

	public int add(Employee employee);

	public Employee find(int eid);

	public List<Employee> list();

}
