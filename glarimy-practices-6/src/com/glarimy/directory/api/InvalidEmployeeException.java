package com.glarimy.directory.api;

public class InvalidEmployeeException extends DirectoryException {
	private static final long serialVersionUID = 5325500363821704943L;

	public InvalidEmployeeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidEmployeeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidEmployeeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidEmployeeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidEmployeeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
