package com.someone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cache<T> {
	private Map<Integer, T> entries = new HashMap<>();

	public void put(int key, T value) {
		entries.put(key, value);
	}

	public T get(int key) {
		return entries.get(key);
	}

	public List<T> all() {
		return new ArrayList<>(entries.values());
	}
}
