package com.glarimy.mediator;

public class Calculator {
	private Mediator mediator;

	public Calculator(Mediator mediator) {
		this.mediator = mediator;
	}

	public int add(int first, int second) {
		mediator.notify("operation: add");
		return first + second;
	}

}
