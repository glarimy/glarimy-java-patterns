package com.glarimy.mediator;

public class Client {
	public static void main(String[] args) throws Exception {
		Mediator mediator = new MessageBus();
		new Biller(mediator);
		new Logger(mediator);
		new Biller(mediator);
		
		Calculator calc = new Calculator(mediator);
		
		System.out.println("Invoking the calc");
		System.out.println("Sum: " + calc.add(23, 45));

		System.out.println("Invoking the calc");
		System.out.println("Sum: " + calc.add(10, 6));

	}
}