package com.glarimy.mediator;

import java.util.ArrayList;
import java.util.List;

public class MessageBus implements Mediator {
	private List<Member> members;

	public MessageBus() {
		members = new ArrayList<>();
	}

	@Override
	public void add(Member member) {
		members.add(member);
	}

	@Override
	public void notify(String message) {
		for(Member member: members)
			member.on(message);
	}
}
