package com.glarimy.mediator;

public interface Member {
	public void service();
	public void on(String message);
}
