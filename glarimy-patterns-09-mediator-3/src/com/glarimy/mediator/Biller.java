package com.glarimy.mediator;

public class Biller implements Member {
	private Mediator mediator;
	private int count;

	public Biller(Mediator mediator) {
		this.mediator = mediator;
		this.mediator.add(this);
	}

	@Override
	public void on(String message) {
		service();
	}

	@Override
	public void service() {
		System.out.println("Biller: " + ++count + "hits");
	}
}
