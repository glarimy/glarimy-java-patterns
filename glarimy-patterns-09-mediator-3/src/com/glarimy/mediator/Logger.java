package com.glarimy.mediator;

import java.util.Date;

public class Logger implements Member {
	private Mediator mediator;

	public Logger(Mediator mediator) {
		this.mediator = mediator;
		this.mediator.add(this);
	}

	@Override
	public void on(String message) {
		try {
			System.out.println("Logger taking some time");
			Thread.sleep(5000);
		} catch (Exception e) {

		}
		service();
	}

	@Override
	public void service() {
		System.out.println("Logger: " + new Date());
	}
}
