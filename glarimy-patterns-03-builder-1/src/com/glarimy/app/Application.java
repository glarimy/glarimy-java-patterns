package com.glarimy.app;

import java.util.Scanner;

import com.glarimy.builder.Order;
import com.glarimy.builder.OrderBuilder;

public class Application {
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Which product");
		int pid = scanner.nextInt();
		System.out.println("Address pls");
		String address = scanner.next();
		OrderBuilder builder = new OrderBuilder(pid, address);
		System.out.println("How many");
		int number = scanner.nextInt();
		builder = builder.quantityOf(number);
		System.out.println("Do you required gift wrapping?");
		String opt = scanner.next();
		if(opt == "yes")
		builder = builder.withGiftWrapping();
		System.out.println("Need prioerty delivery");
		opt = scanner.nextLine();
		if(opt == "yes")
		builder = builder.withGiftWrapping();
		System.out.println("Please wait...");
		Order order = builder.build();
		System.out.println("Here you go!");
		System.out.println(order);
	}
}
