package com.glarimy.factory;

public interface Component {
	public void execute();
}
