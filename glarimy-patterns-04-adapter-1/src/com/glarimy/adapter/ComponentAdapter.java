package com.glarimy.adapter;

public class ComponentAdapter implements Adapter {
	private Component target;

	public ComponentAdapter() {
		this.target = new Component();
	}

	@Override
	public void execute() {
		this.target.process();
	}

}
