package com.glarimy.mediator;

public interface Mediator {

	void add(Member member);

	void notify(String message);

}