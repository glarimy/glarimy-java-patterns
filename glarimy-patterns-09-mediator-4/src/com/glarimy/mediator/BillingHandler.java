package com.glarimy.mediator;

import java.util.concurrent.BlockingQueue;

public class BillingHandler implements Runnable {
	private BlockingQueue<String> queue;
	private int count;

	public BillingHandler(BlockingQueue<String> queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(5000);
			} catch (Exception e) {

			}
			try {
				queue.take();
				System.out.println("Biller: " + ++count + " hits");
			} catch (Exception e) {

			}
		}
	}
}
