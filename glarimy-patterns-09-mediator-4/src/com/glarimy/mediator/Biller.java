package com.glarimy.mediator;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Biller implements Member {
	private BlockingQueue<String> queue;
	private Mediator mediator;

	public Biller(Mediator mediator) {
		this.mediator = mediator;
		this.mediator.add(this);

		this.queue = new ArrayBlockingQueue<>(10);
		Thread t = new Thread(new BillingHandler(queue));
		t.start();
	}

	@Override
	public void on(String message) {
		service();
	}

	@Override
	public void service() {
		queue.add("Operation: Add");
	}
}