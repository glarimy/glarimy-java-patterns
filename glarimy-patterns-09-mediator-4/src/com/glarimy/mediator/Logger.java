package com.glarimy.mediator;

public class Logger implements Member {
	private Mediator mediator;

	public Logger(Mediator mediator) {
		this.mediator = mediator;
		this.mediator.add(this);
	}

	@Override
	public void on(String message) {
		service();
	}

	@Override
	public void service() {
		Thread t = new Thread(new LoggingHandler());
		t.start();
	}

}
