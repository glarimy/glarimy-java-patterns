package com.glarimy.app;

import java.util.Properties;

import com.glarimy.messenger.api.ConfigurableMessengerFactory;
import com.glarimy.messenger.api.Listener;
import com.glarimy.messenger.api.Message;
import com.glarimy.messenger.api.Messenger;
import com.glarimy.messenger.api.MessengerFactory;

public class Application {
	public static void main(String[] args) throws Exception {
		Properties props = new Properties();
		props.setProperty("messenger", "com.glarimy.messenger.service.AsyncMessengerService");
		props.setProperty("logger", "com.glarimy.messenger.service.LoggableMessenger");

		MessengerFactory factory = new ConfigurableMessengerFactory(props);
		Messenger messenger = factory.get();

		Message message = new Message.MessageBuilder("com.glarimy.sports").body("CSK Won");

		messenger.subscribe(new Listener() {
			@Override
			public void on(Message message) {
				System.out.println(message);
			}

			@Override
			public String getTopic() {
				return "com.glarimy.sports";
			}
		});
		messenger.notify(message);
	}
}