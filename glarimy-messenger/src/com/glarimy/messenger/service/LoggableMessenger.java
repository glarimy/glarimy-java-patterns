package com.glarimy.messenger.service;

import java.util.Date;

import com.glarimy.messenger.api.Listener;
import com.glarimy.messenger.api.Message;
import com.glarimy.messenger.api.Messenger;

public class LoggableMessenger implements Messenger {

	private Messenger target;

	public LoggableMessenger(Messenger target) {
		this.target = target;
	}

	@Override
	public String subscribe(Listener listener) {
		System.out.println(new Date() + ": subscribe - entry");
		String response = target.subscribe(listener);
		System.out.println(new Date() + ": subscribe - exit");
		return response;
	}

	@Override
	public void unsubscribe(String sid) {
		System.out.println(new Date() + ": unsubscribe - entry");
		target.unsubscribe(sid);
		System.out.println(new Date() + ": unsubscribe - exit");
	}

	@Override
	public void notify(Message message) {
		System.out.println(new Date() + ": notify - entry");
		target.notify(message);
		System.out.println(new Date() + ": notify - exit");
	}

}
