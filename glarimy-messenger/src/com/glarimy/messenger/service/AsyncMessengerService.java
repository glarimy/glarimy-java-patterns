package com.glarimy.messenger.service;

import com.glarimy.messenger.api.Listener;
import com.glarimy.messenger.api.Message;
import com.glarimy.messenger.api.Singleton;

@Singleton
public class AsyncMessengerService extends AbstractMessenger {
	private static AsyncMessengerService INSTANCE = null;

	private AsyncMessengerService() {
	}

	public synchronized static AsyncMessengerService getInstance() {
		if (INSTANCE == null)
			INSTANCE = new AsyncMessengerService();
		return INSTANCE;
	}

	@Override
	void deliver(Listener listener, Message message) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				listener.on(message);
			}
		}).start();
	}
}
