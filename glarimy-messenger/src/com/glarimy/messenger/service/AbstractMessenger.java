package com.glarimy.messenger.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.glarimy.messenger.api.Listener;
import com.glarimy.messenger.api.Message;
import com.glarimy.messenger.api.Messenger;
import com.glarimy.messenger.api.MessengerException;

public abstract class AbstractMessenger implements Messenger {
	Map<String, List<String>> topics;
	Map<String, Listener> listeners;

	public AbstractMessenger() {
		topics = new HashMap<>();
		listeners = new HashMap<>();
	}

	@Override
	public String subscribe(Listener listener) {
		String topic = listener.getTopic();

		if (topics.get(topic) == null) {
			topics.put(topic, new ArrayList<>());
		}

		String id = new Date().getTime() + "";
		topics.get(topic).add(id);

		listeners.put(id, listener);
		return id;
	}

	@Override
	public void unsubscribe(String sid) {
		Listener listener = listeners.get(sid);
		String topic = listener.getTopic();
		topics.get(topic).remove(sid);
		listeners.remove(sid);
	}

	@Override
	public void notify(Message message) {
		if (message == null)
			throw new MessengerException("invalid message");
		List<String> ids = topics.get(message.getTopic());
		if (ids != null)
			for (String id : ids)
				deliver(listeners.get(id), message);
	}

	abstract void deliver(Listener listener, Message message);
}
