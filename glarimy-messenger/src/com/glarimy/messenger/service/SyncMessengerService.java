package com.glarimy.messenger.service;

import com.glarimy.messenger.api.Listener;
import com.glarimy.messenger.api.Message;
import com.glarimy.messenger.api.Singleton;

@Singleton
public class SyncMessengerService extends AbstractMessenger {
	private static SyncMessengerService INSTANCE = null;

	private SyncMessengerService() {
	}

	public synchronized static SyncMessengerService getInstance() {
		if (INSTANCE == null)
			INSTANCE = new SyncMessengerService();
		return INSTANCE;
	}

	@Override
	void deliver(Listener listener, Message message) {
		listener.on(message);

	}

}
