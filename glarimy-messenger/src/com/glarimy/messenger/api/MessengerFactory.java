package com.glarimy.messenger.api;

public interface MessengerFactory {
	public Messenger get() throws Exception;

}