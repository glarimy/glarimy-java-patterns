package com.glarimy.messenger.api;

public interface Messenger {
	public String subscribe(Listener listner);
	public void unsubscribe(String sid);
	public void notify(Message message);
}
