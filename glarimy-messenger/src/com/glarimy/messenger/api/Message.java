package com.glarimy.messenger.api;

import java.util.Date;

public class Message {
	private String body;
	private String topic;
	private long timeout;
	private long time;
	private String replyTo;

	private Message() {

	}

	public String getBody() {
		return body;
	}

	public String getTopic() {
		return topic;
	}

	public long getTimeout() {
		return timeout;
	}

	public long getTime() {
		return time;
	}

	public String getReplyTo() {
		return replyTo;
	}

	@Override
	public String toString() {
		return "Message [body=" + body + ", topic=" + topic + ", timeout=" + timeout + ", time=" + time + ", replyTo="
				+ replyTo + "]";
	}

	public static class MessageBuilder {
		private String topic;
		private long timeout = 1000;
		private String replyTo;

		public MessageBuilder(String topic) {
			this.topic = topic;
		}

		public MessageBuilder setReplyTo(String topic) {
			this.replyTo = topic;
			return this;
		}

		public MessageBuilder setTimeout(long timeout) {
			this.timeout = timeout;
			return this;
		}

		public Message body(String text) {
			Message message = new Message();
			message.topic = topic;
			message.body = text;
			message.time = new Date().getTime();
			message.timeout = timeout;
			message.replyTo = replyTo;
			return message;
		}
	}
}
