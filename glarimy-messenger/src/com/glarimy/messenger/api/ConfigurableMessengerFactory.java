package com.glarimy.messenger.api;

import java.util.Properties;

public class ConfigurableMessengerFactory implements MessengerFactory {
	private Properties props;

	public ConfigurableMessengerFactory(Properties props) {
		this.props = props;
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Messenger get() throws Exception {
		Class claz = Class.forName(props.getProperty("messenger"));
		Messenger messenger = null;
		if (claz.getAnnotation(Singleton.class) != null)
			messenger = (Messenger) claz.getMethod("getInstance").invoke(claz);
		else
			messenger = (Messenger) claz.newInstance();

		String logger = props.getProperty("logger");
		if (logger != null) {
			Class claas = Class.forName(logger);
			messenger = (Messenger) claas.getConstructor(Messenger.class).newInstance(messenger);
		}
		return messenger;
	}

}
