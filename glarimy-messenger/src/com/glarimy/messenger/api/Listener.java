package com.glarimy.messenger.api;

public interface Listener {
	public void on(Message message);
	public String getTopic();
}
