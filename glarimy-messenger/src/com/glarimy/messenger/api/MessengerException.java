package com.glarimy.messenger.api;

public class MessengerException extends RuntimeException {

	private static final long serialVersionUID = 4609578942674027180L;

	public MessengerException() {
		// TODO Auto-generated constructor stub
	}

	public MessengerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MessengerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MessengerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MessengerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
