package com.glarimy.patterns.client;

import com.glarimy.patterns.api.Component;
import com.glarimy.patterns.service.ComponentBuilder;

public class Application {
	public static void main(String[] args) throws Exception {
		Component comp = new ComponentBuilder().withLogger().withTimer().build();
		comp.service();
	}
}
