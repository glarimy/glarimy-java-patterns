package com.glarimy.patterns.service;

import com.glarimy.patterns.api.Component;

public class Logger implements Component {
	private Component target;

	public Logger(Component target) {
		this.target = target;
	}

	@Override
	public void service() {
		System.out.println("Logger::service::entry");
		target.service();
		System.out.println("Logger::service::exit");

	}
}
