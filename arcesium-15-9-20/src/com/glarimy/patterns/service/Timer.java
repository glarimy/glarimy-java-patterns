package com.glarimy.patterns.service;

import java.util.Date;

import com.glarimy.patterns.api.Component;

public class Timer implements Component {
	private Component target;

	public Timer(Component target) {
		this.target = target;
	}

	@Override
	public void service() {
		long start = new Date().getTime();
		target.service();
		long end = new Date().getTime();
		System.out.println("Timer::service::" + (end - start) + " ms");

	}
}
