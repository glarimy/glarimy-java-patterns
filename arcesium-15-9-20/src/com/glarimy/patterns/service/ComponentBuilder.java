package com.glarimy.patterns.service;

import com.glarimy.patterns.api.Component;
import com.glarimy.patterns.api.ObjectFactory;

public class ComponentBuilder {
	private Component target;

	public ComponentBuilder() throws Exception {
		target = (Component) ObjectFactory.get("component");
	}

	public ComponentBuilder withTimer() {
		target = new Timer(target);
		return this;
	}

	public ComponentBuilder withLogger() {
		target = new Logger(target);
		return this;
	}

	public Component build() {
		return target;
	}
}
