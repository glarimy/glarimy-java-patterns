package com.glarimy.patterns.service;

import com.glarimy.patterns.api.Component;

public class ConcreteComponent implements Component {
	ConcreteComponent() {

	}

	@Override
	public void service() {
		System.out.println("ConcreteComponent::service");
	}
}
