package com.glarimy.patterns.api;

public interface Component {
	public void service();
}
