package com.glarimy.patterns.api;

import java.io.FileReader;
import java.util.Properties;

public class ObjectFactory {
	public static Object get(String key) throws Exception {
		Properties props = new Properties();
		props.load(new FileReader("conf.properties"));
		return Class.forName(props.getProperty(key)).newInstance();
	}
}
