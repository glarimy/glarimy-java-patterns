package com.glarimy.directory.service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.EmployeeNotFoundException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

public class Logger implements Directory {
	private Directory target;

	public Logger(Directory target) {
		this.target = target;
	}

	@Override
	public void add(Employee employee) throws InvalidEmployeeException, DirectoryException {
		System.out.println("Enter: add()");
		target.add(employee);
		System.out.println("Exit: add()");

	}

	@Override
	public Optional<Employee> find(int id) throws EmployeeNotFoundException, DirectoryException {
		System.out.println("Enter: find()");
		Optional<Employee> o = target.find(id);
		System.out.println("Exit: find()");
		return o;

	}

	@Override
	public List<Employee> search(Predicate<Employee> condition) throws DirectoryException {
		System.out.println("Enter: search()");
		List<Employee> list = target.search(condition);
		System.out.println("Exit: search()");
		return list;
	}

	@Override
	public List<Employee> list() throws DirectoryException {
		System.out.println("Enter: list()");
		List<Employee> list = target.list();
		System.out.println("Exit: list()");
		return list;
	}

	@Override
	public List<Long> phones() throws DirectoryException {
		System.out.println("Enter: phones()");
		List<Long> list = target.phones();
		System.out.println("Exit: phones()");
		return list;
	}

}
