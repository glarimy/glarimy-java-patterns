package com.glarimy.directory.service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.EmployeeNotFoundException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

public class Validator implements Directory {
	private Directory target;

	public Validator(Directory target) {
		this.target = target;
	}

	@Override
	public void add(Employee employee) throws InvalidEmployeeException, DirectoryException {
		if (employee == null || employee.getName() == null)
			throw new InvalidEmployeeException();
		target.add(employee);
	}

	@Override
	public Optional<Employee> find(int id) throws EmployeeNotFoundException, DirectoryException {
		return target.find(id);
	}

	@Override
	public List<Employee> search(Predicate<Employee> condition) throws DirectoryException {
		if (condition == null)
			throw new DirectoryException();
		return target.search(condition);
	}

	@Override
	public List<Employee> list() throws DirectoryException {
		return target.list();
	}

	@Override
	public List<Long> phones() throws DirectoryException {
		return target.phones();
	}

}
