package com.glarimy.directory.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.Singleton;
import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

@Singleton(handle = "getInstance")
public class SimpleDirectory implements Directory {
	Map<Integer, Employee> entries = new HashMap<>();

	private static SimpleDirectory INSTANCE = null;

	private SimpleDirectory() {

	}

	public static synchronized SimpleDirectory getInstance() {
		if (INSTANCE == null)
			INSTANCE = new SimpleDirectory();
		return INSTANCE;
	}

	@Override
	public void add(Employee employee) throws InvalidEmployeeException {
		entries.put(employee.getId(), employee);
	}

	@Override
	public Optional<Employee> find(int id) {
		return Optional.of(entries.get(id));
	}

	@Override
	public List<Employee> search(Predicate<Employee> condition) throws DirectoryException {
		return entries.values().stream().filter(condition).collect(Collectors.toList());
	}

	@Override
	public List<Employee> list() throws DirectoryException {
		return new ArrayList<Employee>(entries.values());
	}

	@Override
	public List<Long> phones() throws DirectoryException {
		return entries.values().stream().map(employee -> employee.getPhone()).collect(Collectors.toList());
	}

}
