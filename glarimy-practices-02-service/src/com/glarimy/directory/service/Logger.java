package com.glarimy.directory.service;

import java.util.List;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.EmployeeNotFoundException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

public class Logger implements Directory {
	private Directory target;

	public Logger(Directory target) {
		this.target = target;
	}

	@Override
	public void add(Employee employee) throws InvalidEmployeeException, DirectoryException {
		System.out.println("Enter: add()");
		target.add(employee);
		System.out.println("Exit: add()");

	}

	@Override
	public Employee find(int id) throws EmployeeNotFoundException, DirectoryException {
		System.out.println("Enter: find()");
		Employee e = target.find(id);
		System.out.println("Exit: find()");
		return e;

	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		System.out.println("Enter: search()");
		List<Employee> list = target.search(name);
		System.out.println("Exit: search()");
		return list;
	}

	@Override
	public List<Employee> list() throws DirectoryException {
		System.out.println("Enter: list()");
		List<Employee> list = target.list();
		System.out.println("Exit: list()");
		return list;
	}

}
