package com.glarimy.directory.service;

import java.util.List;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.EmployeeNotFoundException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

public class Validator implements Directory {
	private Directory target;

	public Validator(Directory target) {
		this.target = target;
	}

	@Override
	public void add(Employee employee) throws InvalidEmployeeException, DirectoryException {
		if (employee == null || employee.getName() == null)
			throw new InvalidEmployeeException();
		target.add(employee);
	}

	@Override
	public Employee find(int id) throws EmployeeNotFoundException, DirectoryException {
		return target.find(id);
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		if (name == null || name.trim().length() == 0)
			throw new DirectoryException();
		return target.search(name);
	}

	@Override
	public List<Employee> list() throws DirectoryException {
		return target.list();
	}

}
