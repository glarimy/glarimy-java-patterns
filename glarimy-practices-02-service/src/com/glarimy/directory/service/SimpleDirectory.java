package com.glarimy.directory.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

public class SimpleDirectory implements Directory {
	Map<Integer, Employee> entries = new HashMap<>();

	@Override
	public void add(Employee employee) throws InvalidEmployeeException {
		entries.put(employee.getId(), employee);
	}

	@Override
	public Employee find(int id) {
		return entries.get(id);
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		return entries.values().stream().filter(e -> e.getName().equalsIgnoreCase(name)).collect(Collectors.toList());
	}

	@Override
	public List<Employee> list() throws DirectoryException {
		return new ArrayList<Employee>(entries.values());
	}

}
