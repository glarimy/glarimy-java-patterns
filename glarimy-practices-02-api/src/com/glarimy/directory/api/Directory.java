package com.glarimy.directory.api;

import java.util.List;

import com.glarimy.directory.api.exception.DirectoryException;
import com.glarimy.directory.api.exception.EmployeeNotFoundException;
import com.glarimy.directory.api.exception.InvalidEmployeeException;

public interface Directory {
	void add(Employee employee) throws InvalidEmployeeException, DirectoryException;

	Employee find(int id) throws EmployeeNotFoundException, DirectoryException;

	List<Employee> search(String name) throws DirectoryException;

	List<Employee> list() throws DirectoryException;
}