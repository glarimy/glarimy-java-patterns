package com.glarimy.directory.api;

import java.io.FileReader;
import java.util.Properties;

public class ObjectFactory {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object get(String key) throws Exception {
		Properties props = new Properties();
		props.load(new FileReader("config.properties"));
		String name = props.getProperty(key);
		Class claz = Class.forName(name);
		Object target = claz.newInstance();

		String logger = props.getProperty((key + ".logging"));
		if (logger != null) {
			Class loggerClass = Class.forName(logger);
			Class inf = claz.getInterfaces()[0];
			target = loggerClass.getConstructor(inf).newInstance(target);
		}

		String validator = props.getProperty((key + ".validation"));
		if (validator != null) {
			Class validatorClass = Class.forName(validator);
			Class inf = claz.getInterfaces()[0];
			target = validatorClass.getConstructor(inf).newInstance(target);
		}
		return target;
	}
}
