package com.glarimy.directory.client;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.ObjectFactory;
import com.glarimy.directory.api.domain.Employee;

public class DirectoryApplication {

	public static void main(String[] args) {
		try {
			Directory directory = (Directory) ObjectFactory.get("directory");
			int id = directory.addEmployee(new Employee());
			Employee emp = directory.findEmployee(id);
			System.out.println(emp);
		} catch (Exception de) {
			de.printStackTrace();
		}
	}
}
