package com.glarimy.composit;

public interface Component {
	public void show();
	public void hide();
}
