package com.glarimy.composit;

public interface Container extends Component {
	public void add(Component part);
}
